from django.urls import path
from welcome.views import home

urlpatterns = [
    path("", home, name="welcome"),
]
