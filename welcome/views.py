from django.shortcuts import render

# Create your views here.


def home(request):
    template = "welcome/home.html"
    return render(request, template)
