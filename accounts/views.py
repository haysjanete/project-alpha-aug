from django.shortcuts import redirect
from django.contrib.auth.models import User
from django.views.generic.edit import CreateView
from django.contrib.auth import login
from django.contrib.auth.forms import UserCreationForm

# Create your views here.


class SignUpView(CreateView):
    model = User
    form_class = UserCreationForm
    template_name = "registration/signup.html"

    def form_valid(self, form):
        user = form.instance
        user.save()
        login(self.request, user)
        return redirect("home")
